export default () => {
    document.addEventListener('click', (event) => {
        if(event.target.closest('[data-nav-button]')) {
            document.querySelector('body').classList.toggle('nav-open')
        }
    });

    document.addEventListener('click', (event) => {
        if(event.target.closest('[data-nav-toggle]')) {
            event.preventDefault();
            const navGroup = event.target.closest('[data-nav-parent]')
            const navChildren = navGroup.querySelector('[data-nav-children]')
            navGroup.classList.toggle('open')

            if (navGroup.classList.contains('open')) {
                navChildren.style.maxHeight = navChildren.scrollHeight + 'px';
            } else {
                navChildren.style.maxHeight = null;
            }
        }
    });
};
