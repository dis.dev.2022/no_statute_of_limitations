"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import Navigation from './components/navigation.js';  // Мобильное меню
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import SimpleBar from 'simplebar'; // Кастомный скролл
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import Filter from "./components/filter.js";
import {WebpMachine} from "webp-hero" // WebP support
import AirDatepicker from 'air-datepicker';
import AOS from  'aos'; // AOS animation

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
Navigation();

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки

if (document.querySelectorAll('[data-simplebar]').length) {
    document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
        new SimpleBar(scrollBlock, {
            autoHide: false
        });
    });
}

// Animation
(() => {
    let breakpoint  = 1230;
    let isAos = null;

    function newAOS() {
        if(!isAos) {
            AOS.init({
                duration: 1000,
            });
        }
    }
    function destroyAOS() {
        if(isAos) {
        }
    }

    if (document.documentElement.clientWidth > breakpoint) {
        newAOS()
    }
    window.addEventListener('resize', function (){

        if (document.documentElement.clientWidth > breakpoint) {
            newAOS()
        }
        else {
            destroyAOS()
        }
    });
})();


// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

// Sliders
import "./components/sliders.js";

let Parallax = () => {
    const counters = document.querySelectorAll('[data-counter]')
    if (counters.length > 0) {
        counters.forEach(elem => {
            let counterNum =  parseInt(elem.dataset.counter)
            let counterStep =  parseInt(elem.dataset.step)
            let counterTime =  parseInt(elem.dataset.time)

            let n = 0;
            let t = Math.round(counterTime / (counterNum / counterStep));

            console.log(counterNum, counterStep, counterTime)

            let interval = setInterval(() => {
                n = n + counterStep;
                if (n === counterNum) {
                    clearInterval(interval);
                }
                elem.innerHTML = n;
            }, t);
        });
    }
}

function intersectRect(r1, r2) {
    return !(r2.left > r1.right || r2.right < r1.left ||
        r2.top > r1.bottom || r2.bottom < r1.top);
}

(() => {

    if (document.querySelector('[data-solder-one]')) {
        window.addEventListener('mousemove', function(e) {
            let x = e.clientX / window.innerWidth;
            let y = e.clientY / window.innerHeight;
            document.querySelector('[data-solder-one]')
                .style.transform = 'translate(-' + x * 50 + 'px, -' + y * 50 + 'px)';
        });
    }

    if (document.querySelector('[data-solder-two]')) {
        window.addEventListener('mousemove', function(e) {
            let x = e.clientX / window.innerWidth;
            let y = e.clientY / window.innerHeight;
            document.querySelector('[data-solder-two]').
                style.transform = 'translate(' + x * 40 + 'px, -' + y * 60 + 'px)';
        });
    }

    if (document.querySelector('[data-solder-bg]')) {
        window.addEventListener('mousemove', function(e) {
            let x = e.clientX / window.innerWidth;
            let y = e.clientY / window.innerHeight;
            document.querySelector('[data-solder-bg]')
                .style.transform = 'translate(' + x * 20 + 'px, -' + y * 30 + 'px)';
        });
    }

})();

// filter
Filter()

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-filter-value]')) {
        event.target.closest('[data-dropdown]').classList.remove('open')
    }
})

// Input Date {
document.querySelectorAll('[data-date-range]').forEach(el => {
    new AirDatepicker(el,{
        autoClose: true,
        range: true,
        position: 'left top',
        multipleDatesSeparator: ' - ',
    });
});


document.querySelectorAll('[data-calendar]').forEach(el => {
    new AirDatepicker(el,{
        autoClose: true,
        range: true,
        multipleDatesSeparator: ' - ',

        onSelect({date, formattedDate, datepicker}) {
            document.querySelectorAll('[data-dropdown]').forEach(el => {
                el.classList.remove('open');
            });

        }
    });
});


document.querySelectorAll('[data-calendar-inline]').forEach(el => {
    new AirDatepicker(el,{
        inline: true,
        autoClose: true,
        range: true,
        multipleDatesSeparator: ' - ',
    });
});


// Event Calendar

(() => {
    if (document.querySelector('[data-events]')) {
        const events = document.querySelector('[data-events]')

        const picker = document.querySelector('[data-events-picker]')
        new AirDatepicker(picker,{
            inline: true,
            selectedDates: ['2022-10-18']
        });

        document.addEventListener('click', (event) => {
            if(event.target.closest('[data-events-toggle]')) {
                events.classList.toggle('open');

                if (events.classList.contains('open')) {
                    document.querySelector('#events-date').scrollIntoView({
                        behavior: 'smooth',
                        block: 'start'
                    });
                }
            }
        })
    }
})();

// Docs

(() => {
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-docs-button]')) {
            const docs = event.target.closest('[data-docs]');
            const docsNavs = docs.querySelectorAll('[data-docs-button]');
            const docsContent = docs.querySelectorAll('[data-docs-group]');
            const docsPath = event.target.closest('[data-docs-button]').dataset.docsButton;

            docsNavs.forEach(elem => {
                elem.classList.remove('active');
            });
            docsContent.forEach(elem => {
                elem.classList.remove('active');
            });
            docs.querySelector(`[data-docs-button="${docsPath}"]`).classList.add('active');
            docs.querySelector(`[data-docs-group="${docsPath}"]`).classList.add('active');
        }
    });
})();

window.addEventListener("load", function (e) {

    if(document.querySelector('[data-counters]')) {
        let container = document.querySelector('body');
        let containerRect = document.body.getBoundingClientRect();
        let trackElement = document.querySelector('[data-counters]')

        container.onscroll = function() {
            let r = trackElement.getBoundingClientRect();
            if (intersectRect(r, containerRect) && container.last !== trackElement) {
                container.last = trackElement;
                Parallax()
            }
        }
    }
});

function inputFile(selector = '[data-file-value]') {
    const inputFiles = document.querySelectorAll(selector)
    if (inputFiles.length > 0) {

        inputFiles.forEach(elem => {
            elem.addEventListener('change', (event) => {
                let item = event.target.closest('[data-file]')
                let str = elem.value
                let placeholder = item.querySelector('[data-file-label]').dataset.fileLabel
                let i, filename

                if (str.lastIndexOf('\\')){
                    i = str.lastIndexOf('\\')+1;
                }
                else {
                    i = str.lastIndexOf('/')+1;
                }
                filename = str.slice(i);

                if (filename) {
                    item.classList.add('-selected-')
                    item.querySelector('[data-file-label]').innerHTML = filename
                }
                else {
                    item.classList.remove('-selected-')
                    item.querySelector('[data-file-label]').innerHTML = placeholder
                }
            });
        });
    }
}

inputFile()
