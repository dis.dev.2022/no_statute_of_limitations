/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper from 'swiper';
import { Autoplay, Navigation, Pagination, EffectFade,EffectCreative, Thumbs } from 'swiper/modules';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('[data-swiper]')) {
        new Swiper('[data-swiper]', {
            // Подключаем модули слайдера
            // для конкретного случая
            //modules: [Navigation, Pagination],
            /*
            effect: 'fade',
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            */
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            autoHeight: true,
            speed: 800,
            //touchRatio: 0,
            //simulateTouch: false,
            //loop: true,
            //preloadImages: false,
            //lazy: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '.swiper__more .swiper__more--next',
                prevEl: '.swiper__more .swiper__more--prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 16,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 32,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-primary-bg]')) {
        let primaryBg = new Swiper('[data-primary-bg]', {
            modules: [Autoplay, EffectFade],
            loop: true,
            speed: 1000,
            loopedSlides: 1,
            slidesPerView: 1,
            spaceBetween: 0,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
        });
    }

    if (document.querySelector('[data-preview]')) {
        let previewQuerySize  = 992;
        let previewSlider = null;

        function previewSliderInit() {
            if(!previewSlider) {
                previewSlider = new Swiper('[data-preview]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 8,
                });
            }
        }

        function previewSliderDestroy() {
            if(previewSlider) {
                previewSlider.destroy();
                previewSlider = null;
            }
        }

        if (document.documentElement.clientWidth < previewQuerySize) {
            previewSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < previewQuerySize) {
                previewSliderInit()
            }
            else {
                previewSliderDestroy()
            }
        });
    }

    if (document.querySelector('[data-intro]')) {

        let newsQuerySize  = 1230;
        let newsSlider = null;

        function newsSliderInit() {
            if(!newsSlider) {
                newsSlider = new Swiper('[data-intro]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                });
            }
        }

        function newsSliderDestroy() {
            if(newsSlider) {
                newsSlider.destroy();
                newsSlider = null;
            }
        }

        if (document.documentElement.clientWidth < newsQuerySize) {
            newsSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < newsQuerySize) {
                newsSliderInit()
            }
            else {
                newsSliderDestroy()
            }
        });
    }

    if (document.querySelector('[data-tags-line]')) {
        new Swiper('[data-tags-line]', {
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 0,
        });
    }

    if (document.querySelector('[data-quotes]')) {
        new Swiper('[data-quotes]', {
            modules: [Navigation, Pagination],
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: '1',
            spaceBetween: 40,
            navigation: {
                nextEl: '[data-quotes-next]',
                prevEl: '[data-quotes-prev]',
            },
            pagination: {
                el: '[data-quotes-pagination]',
                clickable: true,
            },
        });
    }

    if (document.querySelector('[data-stat-media]')) {
        let statQuerySize  = 1230;
        let statSlider = null;

        function statSliderInit() {
            if(!statSlider) {
                statSlider = new Swiper('[data-stat-media]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                });
            }
        }

        function statSliderDestroy() {
            if(statSlider) {
                statSlider.destroy();
                statSlider = null;
            }
        }

        if (document.documentElement.clientWidth < statQuerySize) {
            statSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < statQuerySize) {
                statSliderInit()
            }
            else {
                statSliderDestroy()
            }
        });
    }

    if (document.querySelector('[data-news-sidebar]')) {

        let newsQuerySize  = 1230;
        let newsSlider = null;

        function newsSliderInit() {
            if(!newsSlider) {
                newsSlider = new Swiper('[data-news-sidebar]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                });
            }
        }

        function newsSliderDestroy() {
            if(newsSlider) {
                newsSlider.destroy();
                newsSlider = null;
            }
        }

        if (document.documentElement.clientWidth < newsQuerySize) {
            newsSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < newsQuerySize) {
                newsSliderInit()
            }
            else {
                newsSliderDestroy()
            }
        });
    }

    if (document.querySelector('[data-media]')) {
        new Swiper('[data-media]', {
            modules: [Navigation],
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 16,
            navigation: {
                nextEl: '[data-media-next]',
                prevEl: '[data-media-prev]',
            },
        });
    }

    if (document.querySelector('[data-docs-nav]')) {
        new Swiper('[data-docs-nav]', {
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 16,
        });
    }

    if (document.querySelector('[data-exhibition]')) {
        new Swiper('[data-exhibition]', {
            modules: [Navigation],
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 16,
            navigation: {
                nextEl: '[data-exhibition-next]',
                prevEl: '[data-exhibition-prev]',
            },
        });
    }

    if (document.querySelector('[data-research]')) {
        new Swiper('[data-research]', {
            modules: [Navigation],
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 24,
            navigation: {
                nextEl: '[data-research-next]',
                prevEl: '[data-research-prev]',
            },
            breakpoints: {
                1230: {
                    slidesPerView: 3,
                    spaceBetween: 24,
                },
                1680: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                },
            },
        });
    }

    if (document.querySelector('[data-experts]')) {

        let expertsQuerySize  = 1230;
        let expertsSlider = null;

        function expertsSliderInit() {
            if(!expertsSlider) {
                expertsSlider = new Swiper('[data-experts]', {
                    observer: true,
                    modules: [Pagination],
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                    pagination: {
                        el: "[data-experts-pagination]",
                        clickable: true,
                    },
                });
            }
        }


        function expertsSliderDestroy() {
            if(expertsSlider) {
                expertsSlider.destroy();
                expertsSlider = null;
            }
        }

        if (document.documentElement.clientWidth < expertsQuerySize) {
            expertsSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < expertsQuerySize) {
                expertsSliderInit()
            }
            else {
                expertsSliderDestroy()
            }
        });
    }


    if (document.querySelector('[data-comments]')) {
        new Swiper('[data-comments]', {
            modules: [Navigation, Pagination],
            loop: false,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 4,
            navigation: {
                nextEl: '[data-comments-next]',
                prevEl: '[data-comments-prev]',
            },
            pagination: {
                el: "[data-comments-pagination]",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: "auto",
                    spaceBetween: 24,
                },
            },
        });
    }
}




window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
