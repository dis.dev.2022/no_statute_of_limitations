export default () => {

    document.addEventListener('click', (event) => {
        const dropdown = document.querySelectorAll('[data-dropdown]')
        if (dropdown.length > 0) {
            if (event.target.closest('[data-dropdown-toggle]')) {
                let dropdownContainer = event.target.closest('[data-dropdown]')
                if (dropdownContainer.classList.contains('open')) {
                    dropdownContainer.classList.remove('open')
                }
                else {
                    dropdownClose();
                    dropdownContainer.classList.add('open')
                }
            }
            else if (event.target.closest('[data-dropdown]')) {

            }
            else {
                dropdownClose();
            }
            function dropdownClose() {
                dropdown.forEach(el => {
                    el.classList.remove('open')
                })
            }
        }
    })

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-select-item]')) {
            const item = event.target.closest('[data-select]')
            const itemValue = event.target.closest('[data-select-item]').dataset.selectItem
            const itemActive = item.querySelector('[data-select-active]')

            itemActive.innerHTML = itemValue
            item.classList.remove('open')
        }
    })
};


