export default () => {

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-value]')) {
            event.target.closest('[data-dropdown]').classList.remove('open')
        }
    })

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-toggle]')) {
            event.target.closest('body').classList.toggle('filter-open')
        }
    })

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-item]')) {
            let filter = event.target.closest('[data-filter-mobile]')
            let filterPrimary = event.target.closest('[data-filter-primary]')
            let item =   event.target.closest('[data-filter-item]')
            const filterSecondary = item.dataset.filterItem;

            filterPrimary.classList.add('hide')
            filter.querySelector(`[data-filter-secondary="${filterSecondary}"]`).classList.add('active');
            console.log(filterSecondary);
        }
    })

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-back]')) {
            event.target.closest('[data-filter-mobile]').querySelector('[data-filter-primary]').classList.remove('hide')
            event.target.closest('[data-filter-secondary]').classList.remove('active');
        }
    })

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-submit]')) {
            event.target.closest('[data-filter-mobile]').querySelector('[data-filter-primary]').classList.remove('hide')
            event.target.closest('[data-filter-secondary]').classList.remove('active');
            event.target.closest('body').classList.remove('filter-open')
        }
    })

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-reset]')) {
            event.target.closest('body').classList.remove('filter-open')
        }
    })


    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-list] li')) {
            const itemArray = event.target.closest('[data-filter-list]').querySelectorAll('li')

            itemArray.forEach(elem => {
                elem.classList.remove('active');
            });
            event.target.closest('[data-filter-list] li').classList.add('active')
        }
    })
};



